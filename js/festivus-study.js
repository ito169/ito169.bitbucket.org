(function () {
  if (!location.origin) {
    location.origin = location.protocol + "//" + location.host;
  }

  var seasonsTemplate = new EJS({ url: 'views/seasons.ejs' });

  $.getJSON(location.origin + '/json/festivus-study.json', function (studyData) {
    var seasonsView = seasonsTemplate.render({
      seasons: studyData.seasons,
      episodes: studyData.episodes,
      notes: studyData.notes,
      people: studyData.people,
      characters: studyData.characters,
      quotes: studyData.quotes,
      roles: studyData.roles,
    });

    $('body').append(seasonsView);

    bindDropdownBehavior('.season', '.episodes');
    bindDropdownBehavior('.episode', '.episode-info');
    bindDropdownBehavior('.episode-note', '.episode-notes');
    bindDropdownBehavior('.regular-characters', '.regular-character-roles');
    bindDropdownBehavior('.guest-characters', '.guest-character-roles');
    bindDropdownBehavior('.episode-quotes', '.episode-quote-roles');
  })
  .fail(function (d, textStatus, error) {
    console.log('textStatus:', textStatus);
    console.log('error:', error);
  });

  function bindDropdownBehavior(clickClass, dropdownClass) {
    $(clickClass).on('click touch', function (event) {
      event.preventDefault();

      $(clickClass).not($(event.currentTarget)).siblings().hide('slow');

      $(event.currentTarget).siblings(dropdownClass).slideToggle('slow');
    });
  };
})();
